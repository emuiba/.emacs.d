;;; setup-c.el --- 


;; C style
(setq c-default-style "linux"
      c-basic-offset 4)

(defun delete-backward-indent (&optional arg)
  "Erase a level of indentation, or 1 character"
  (interactive "*P")
  (if (= (current-column) 0)
      (delete-backward-char 1)
    (let ((n (mod (current-column) standard-indent)))
      (if (looking-back (concat "^\s*" (make-string n 32)))
          (if (= n 0)
              (delete-backward-char standard-indent)
            (delete-backward-char n))
        (delete-backward-char 1)))))


(add-hook 'c-mode-common-hook
          '(lambda ()
             (hs-minor-mode 1)
             (define-key c-mode-base-map (kbd "RET") 'newline-and-indent)
             (define-key c-mode-base-map (kbd "DEL") 'delete-backward-indent)
             (define-key c-mode-base-map (kbd "M-RET") 'hs-toggle-hiding)
             (key-chord-define c-mode-base-map ";;" "\C-e;")
             (local-unset-key "\C-d")
                                        ;            (fci-mode 1)
             (show-ws-toggle-show-trailing-whitespace)
             ))

(dolist (mode '(c-mode
                java-mode
                perl-mode
                html-mode-hook
                css-mode-hook
                emacs-lisp-mode))
  (font-lock-add-keywords mode
                          '(("\\(FIXME\\|TODO\\)"
                             1 font-lock-warning-face prepend)))
;;  (fci-mode 1)
  (setq fci-rule-color "darkgray")
  )


(provide 'setup-c)
;;; setup-c.el ends here
