;;; init.el ---

;;to measure how long it takes to process this file
(setq emacs-load-start-time (current-time))

;; Turn off mouse interface early in startup to avoid momentary display
(dolist (mode '(menu-bar-mode tool-bar-mode scroll-bar-mode))
  (when (fboundp mode) (funcall mode -1)))


;; ;; Set path to .emacs.d
(setq dotfiles-dir "~/.emacs.d")

;; Set path to dependencies
(setq site-lisp-dir (expand-file-name "site-lisp" dotfiles-dir))
(setq elpa-dir (expand-file-name "elpa" dotfiles-dir))

;; Set up load path
(add-to-list 'load-path dotfiles-dir)
(add-to-list 'load-path site-lisp-dir)
(add-to-list 'load-path elpa-dir)

;; Add external projects to load path
(dolist (project (directory-files site-lisp-dir t "\\w+"))
  (when (file-directory-p project)
    (add-to-list 'load-path project)))


;; Add external projects to load path
(dolist (project (directory-files elpa-dir t "\\w+"))
  (when (file-directory-p project)
    (add-to-list 'load-path project)))

;;------------------------------------------------------------------------
;; GUI
;;-------------------------------------------------------------------------


(require 'mac)
(require 'flymake)
(require 'flymake-cursor)
(require 'appearance)
(require 'sane-defaults)
(require 'setup-ido)
(require 'setup-magit)
(require 'setup-dired)
(require 'setup-shell)
(require 'mode-mappings)
(require 'setup-perl)
(require 'setup-yasnippet)
;;(require 'setup-python)
(require 'key-bindings)
(require 'smex)
(smex-initialize)
(require 'miscelanea)
(require 'setup-tramp)
(require 'setup-c)
(require 'setup-org-mode)


(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/dict")
(ac-config-default)
                                        ;(require 'auto-complete)
;; highlight the current window
(require 'idle-highlight-mode)
(idle-highlight-mode t)



(message "Emacs startup time: %f seconds."
         (time-to-seconds (time-since emacs-load-start-time)))
(sit-for 1.5)
(put 'erase-buffer 'disabled nil)



(setq font-lock-verbose nil)
(put 'ido-exit-minibuffer 'disabled nil)
(put 'upcase-region 'disabled nil)
