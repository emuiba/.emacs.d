;;; mode-mappings.el --- 



;; Perl
(add-to-list 'auto-mode-alist '("\\.pl\\'"   . perl-mode))
(add-to-list 'auto-mode-alist '("\\.pm\\'"   . perl-mode))
(add-to-list 'auto-mode-alist '("\\.t\\'"   . perl-mode))

;; org-mode
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))


;; Apache config
(add-to-list 'auto-mode-alist '("\\.htaccess\\'"   . apache-mode))
(add-to-list 'auto-mode-alist '("httpd\\.conf\\'"  . apache-mode))


(provide 'mode-mappings)
;;; mode-mappings.el ends here
