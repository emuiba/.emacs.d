;;; setup-doc-view-mode.el --- 

(defun doc-view-scroll-up-or-next-page+ (&optional reverse)
  "Get next page in `doc-view-mode' buffer in other window.
Optional argument REVERSE default is scroll up (or next page), if REVERSE is non-nil scroll down (or previous page)."
  (interactive)
  (catch 'found
    (walk-windows
     (lambda (w)
       (with-selected-window w
         (when (eq major-mode 'doc-view-mode)
           (if reverse
               (doc-view--previous-page)
             (doc-view-next-page))
           (throw 'found "Have found")))))))



(provide 'setup-doc-view-mode)
;;; setup-doc-view-mode.el ends here
