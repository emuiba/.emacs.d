;;; setup-org-mode.el ---

(require 'org-install)

;; be able to change page on the doc-view buffer
(defun doc-view-scroll-next-page+ (&optional reverse)
  "Get next page in `doc-view-mode' buffer in other window.  Optional
argument REVERSE default is next page, if REVERSE is non-nil previous
page."
  (interactive)
  (catch 'found
    (walk-windows
     (lambda (w)
       (with-selected-window w
         (when (eq major-mode 'doc-view-mode)
           (doc-view-next-page)
           (throw 'found "Have found")))))))

;; FIX ME (using reverse) http://www.emacswiki.org/emacs/doc-view-extension.el

;; be able to change page on the doc-view buffer
(defun doc-view-scroll-previous-page+ (&optional reverse)
  "Get next page in `doc-view-mode' buffer in other window.  Optional
argument REVERSE default is next page, if REVERSE is non-nil previous
page."
  (interactive)
  (catch 'found
    (walk-windows
     (lambda (w)
       (with-selected-window w
         (when (eq major-mode 'doc-view-mode)
           (doc-view-previous-page)
           (throw 'found "Have found")))))))

(add-hook 'org-load-hook
          (lambda ()
            (define-key org-mode-map "\C-n" 'doc-view-scroll-next-page+)
            (define-key org-mode-map "\C-p" 'doc-view-scroll-previous-page+)))

;; visual-mode for org files
(add-hook 'org-mode-hook
          '(lambda() (visual-line-mode t)))




(setq org-replace-disputed-keys t)


(provide 'setup-org-mode)
;;; setup-org-mode.el ends here
