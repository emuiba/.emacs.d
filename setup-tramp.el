;;; setup-tramp.el --- 

(require 'tramp)
(setq tramp-default-method "ssh")


(provide 'setup-tramp)
;;; setup-tramp.el ends here
