;;; setup-python.el ---

(autoload 'pymacs "~/.emacs.d/Pymacs/pymacs.el" "Syntax checker" t)
                                        ;(pymacs-load "ropemacs" "rope-")
                                        ;(setq ropemacs-enable-autoimport t)



(load-file "~/.emacs.d/site-lisp/emacs-for-python/epy-init.el")
;; (eval-after-load "python-mode" '(load "~/.emacs.d/site-lisp/emacs-for-python/epy-init.el"))

;; (eval-after-load "python-mode" '(epy-setup-checker "pyflakes %f"))
;; (eval-after-load "python-mode" '(epy-setup-ipython))

(epy-setup-checker "pyflakes %f")

;;(epy-django-snippets)
(epy-setup-ipython)


(provide 'setup-python)
;;; setup-python.el ends here
