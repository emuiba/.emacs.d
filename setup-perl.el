;;; setup-perl.el ---


(add-to-list 'load-path (expand-file-name "Emacs-PDE-0.2.16/lisp" site-lisp-dir))

;; Use cperl-mode instead of the default perl-mode
(defalias 'perl-mode 'cperl-mode)
;;(setq sepia-perl5lib (list (expand-file-name "~/.emacs.d/Sepia-0.992/lib")))
;; (defalias 'perl-mode 'sepia-mode)
;; (require 'sepia)
(add-hook 'cperl-mode-hook
          '(lambda ()
             ;; (setq pde-perl-version "5.14.2")
             ;; (abbrev-mode t)
                                        ;            (require 'perl-completion)
                                        ;            (perl-completion-mode t)
             ;; (make-variable-buffer-local 'ac-sources)
             ;; (setq ac-sources '(ac-source-perl-completion))
             ;; (set (make-local-variable 'eldoc-documentation-function)
             ;;      'my-cperl-eldoc-documentation-function)
             (local-set-key (kbd "C-h f") 'perldoc)
                                        ;            (cperl-set-style "PerlStyle")
             (setq cperl-auto-newline nil)
             ;; (setq cperl-electric-keywords nil)
             ;; (setq cperl-electric-parens nil)
             (local-set-key (kbd "C-c C-r") 'inf-perl-send)
             (cperl-auto-newline-after-colon nil)
             (flymake-mode t)
             ))

;(autoload "pde-load" "pde-load" "pde" t)
;;(load "pde-load")
(eval-after-load "cperl-mode" '(load "pde-load"))

(setq auto-mode-alist (append '(("\\.pl$" . perl-mode))
                              auto-mode-alist))



;; (require 'perlbrew-mini)
;; (perlbrew-mini-set-perls-dir "/home/edu/perl5/perlbrew/perls/")
;; ;; change the version you wish to use
;; (perlbrew-mini-use "perl-5.16.0")


;; (defun flymake-perl-init ()
;;   (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;                      'flymake-create-temp-inplace))
;;          (local-file (file-relative-name
;;                       temp-file
;;                       (file-name-directory buffer-file-name))))
;;     (list (perlbrew-mini-get-current-perl-path)
;;           (list "-MProject::Libs" "-wc" local-file)))
;;   )



(provide 'setup-perl)
;;; setup-perl.el ends here
