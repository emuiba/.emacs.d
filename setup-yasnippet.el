;;; setup-yasnippet.el ---

(require 'yasnippet) ;; not yasnippet-bundle

;;(yas/initialize)
(yas/load-directory "~/.emacs.d/site-lisp/yasnippet/snippets")
(yas/global-mode 1)

(provide 'setup-yasnippet)
;;; setup-yasnippetel ends here
