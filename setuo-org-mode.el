;;; setup-org-mode.el --- 

(require 'org-install)


;; visual-mode for org files
(add-hook 'org-mode-hook
          '(lambda() (visual-line-mode t)))

(setq org-replace-disputed-keys t)


(provide 'setup-org-mode)
;;; setup-org-mode.el ends here
